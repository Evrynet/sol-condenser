# sol-condenser

An open source utility that helps trading bots (utilizing the `floodgate` API) construct a [solana transaction](https://docs.solana.com/developing/programming-model/transactions) that encapsulates a [serum DEX](https://docs.google.com/document/d/1isGJES4jzQutI0GtQGuqtrBUqeHxl_xJNXdtOv4SdII/edit#) order.

